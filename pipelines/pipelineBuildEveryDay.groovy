def relativeJunitLogsPath = '/core-customize/hybris/log/junit'
/*def projectDir = "${env.WORKSPACE}"*/
//echo "projectdir ${projectDir}"
pipeline {
    libraries {
        lib("shared-library@${params.LIBRARY_BRANCH}")
    }
   /* agent {
        node {
            label 'master'
        }
    }*/
    agent any
    
       triggers {
        cron('H 18 * * *')
    }
    options {
        skipDefaultCheckout(true) // No more 'Declarative: Checkout' stage
    }

    stages {
        stage('Prepare') {
            steps { echo "pipelinestarted"
                script {
                    projectDir = "${env.WORKSPACE}"
                }
                cleanWs()
                /*checkoutRepository("${projectDir}", "${params.PROJECT_TAG}", "${params.PROJECT_REPO}")*/
                checkoutRepository("${WORKSPACE}", "${params.PROJECT_TAG}", "${params.PROJECT_REPO}")
                //extractCommerce(projectDir)
                extractCommerce("${WORKSPACE}")
            }
        }

        stage('Platform Setup') {
            steps {
                script {
                    executeCommerceBuild("${WORKSPACE}")
                }
            }
        }

        stage('Run sonarqube') {
            steps {
                //sonarqubeCheck("${BUILD_TAG}_develop", projectDir, "${params.SONAR_REPO_NAME}", "${params.SONAR_URL}") // Pipeline status is set as UNSTABLE if Sonar Quality Gate fails but build is SUCCESSFUL
                sonarqubeCheck("${BUILD_TAG}_develop", "${WORKSPACE}", "${params.SONAR_REPO_NAME}", "${params.SONAR_URL}")
                failIfBuildUnstable() // Fails build if Quality Gate fails
            }
        }

        stage('Run all tests') {
            steps {
                //executeAntTasks(projectDir, "yunitinit alltests -Dtestclasses.packages=${params.PACKAGE_TO_TEST}", 'dev')
                executeAntTasks("${WORKSPACE}", "yunitinit alltests -Dtestclasses.packages=${params.PACKAGE_TO_TEST}", 'master')
            }
        }
    }

     //post build actions
    post {
       always {
           junit "${relativeJunitLogsPath}/*.xml"
        }
    }
}
